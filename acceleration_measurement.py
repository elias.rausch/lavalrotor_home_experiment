import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "/home/pi/lavalrotor_home_experiment/datasheets/setup_compressor.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="/home/pi/lavalrotor_home_experiment/datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)

# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Erstelle ein leeres Dictionary für die Sensor-Daten
sensor_data = {}



# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
messdauer = 10  # Zum Beispiel 10 Sekunden

# Simuliere das Befüllen der Sensordaten für die definierte Messdauer
start_time = time.time()
data={sensor_settings_dict["ID"]:{"acceleration_x":[],"acceleration_y":[],"acceleration_z":[],"timestamp":[]}}
while (time.time() - start_time) < messdauer:
    # Annahme: Hier werden Daten von Sensoren gelesen
    # Beispiel: Zufällige Daten für die Beschleunigungssensoren generieren
    temp=accelerometer.acceleration
    data[sensor_settings_dict["ID"]]["acceleration_x"].append(temp[0])
    data[sensor_settings_dict["ID"]]["acceleration_y"].append(temp[1])
    data[sensor_settings_dict["ID"]]["acceleration_z"].append(temp[2])
    data[sensor_settings_dict["ID"]]["timestamp"].append(time.time())  # Zeitstempel hinzufügen
    
    time.sleep(0.001)  # 1 ms Pause
    
print(data)

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# *metadata = {sensor_uuid: {key: value["unit"] for key, value in data.items()} for sensor_uuid, data in sensor_data.items()}

metadata_file_path = "./metadata2.json"

with open(metadata_file_path, "w") as metadata_file:
    json.dump(data, metadata_file, indent=4)

# ---------------------------------------------------------------------------------------------#4-end

# """Log JSON metadata"""
# log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
# print("Measurement data was saved in {}/".format(path_measurement_folder))