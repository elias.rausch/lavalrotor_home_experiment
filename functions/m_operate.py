import json
import time
import os
import h5py
import shutil
from typing import Tuple, Dict


def prepare_metadata(
    setup_json_path: str, path_folder_metadata: str = "datasheets"
) -> Tuple[Dict, Dict, str, str]:
    """Prepare JSON metadata for measurement at laval-rotor home experiment.

    Args:
        setup_json_path (str): Path to the JSON setup file.
        folder_path_metadata (str, optional): Path to the folder in which the
            metadata referenced in setup_json_path is stored. Defaults to "datasheets".

    Returns:
        setup_json_dict (Dict): Information from the setup_json_path file,
            enriched with the paths to the JSON files of the components.
        sensor_info_dict (Dict): All relevant metadata of the ADXL345 sensor.
        h5_file_path (str): Path to the h5 file that was created to save the measurement data.
        path_measurement_folder (str): Path to the folder in which the measurement data is to be saved.
    """
    setup_json_dict = evaluate_setup_json(setup_json_path, path_folder_metadata)
    probe_name = get_probe_info(setup_json_dict)
    sensor_info_dict = get_sensor_info(setup_json_dict)
    general_info_dict = get_general_info(setup_json_dict)

    safeNames = generate_safe_name(
        researcher=general_info_dict["researcher"], probe=probe_name
    )
    h5_file_path, path_measurement_folder = createH5File(
        safeNames["filename"], general_info_dict, safeNames["foldername"]
    )

    return setup_json_dict, sensor_info_dict, h5_file_path, path_measurement_folder


def evaluate_metadata(folder_path_metadata: str) -> Tuple[Dict, Dict, str]:
    """Prepare JSON metadata for post-processing in laval-rotor home experiment.

    Args:
        folder_path_metadata (str): Path to the folder in which the 
        data is saved.

    Returns:
        setup_json_dict (Dict): Information from JSON setup file of the analysed measurement data.
        sensor_info_dict (Dict): ADXL345 sensor-metadata.
        probe_name (str): Name of the analysed probe, as specified in the JSON setup file.
    """
    
    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith(".json"):
                
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    json_content = json.load(json_file)
                    
                    if "setup" in json_content.keys():
                        setup_json_path = file_path
    setup_json_dict = evaluate_setup_json(setup_json_path, folder_path_metadata)
    probe_name = get_probe_info(setup_json_dict)
    sensor_info_dict = get_sensor_info(setup_json_dict)
    return setup_json_dict, sensor_info_dict, probe_name


def evaluate_setup_json(
    setup_json_path: str, folder_path_metadata: str = "datasheets"
) -> Dict:
    """Scans all metadata specified in the file with the path equal to setup_json_path.

    Args:
        setup_json_path (str): Path to JSON setup file.
        folder_path_metadata (str): Path where the JSON files of the components are located.
            Defaults to "datasheets".

    Raises:
        RuntimeError: Is triggered if the information in the component JSON file does not match the information
            in the JSON-Setup file.

    Returns:
        Dict: Information from the setup_json_path file, enriched with the paths to the JSON files of the components.
    """
    with open(setup_json_path, "r") as json_file:
        json_content = json.load(json_file)

    setup_dict = json_content["setup"]

    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith((".json")):
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    try:
                        json_content = json.load(json_file)
                    except json.JSONDecodeError:
                        raise RuntimeError(
                            "[ERROR] Please check the syntax in the json file: {}".format(
                                file_path
                            )
                        )
                    json_file_uuid = json_content["JSON"]["ID"]
                if json_file_uuid in setup_dict.keys():
                    json_file_type = list(json_content.keys())[1]
                    if setup_dict[json_file_uuid]["type"] == json_file_type:
                        setup_dict[json_file_uuid]["path"] = file_path
                    else:
                        print(
                            "Something is wrong with key in [{}] or the corresponding entry in the JSON setup file. [{}]".format(
                                file_path, setup_json_path
                            )
                        )
                        raise RuntimeError(
                            "Metadata in JSON files is not specified correctly."
                        )

    return setup_dict


def get_sensor_info(setup_dict: Dict) -> Dict:
    """Get information about ADXL345 sensor from JSON setup file.

    Args:
        setup_dict (Dict): Contains information from JSON setup file.

    Raises:
        RuntimeError: Occurs if no suitable information was found in the JSON setup file.

    Returns:
        Dict: All relevant metadata of the ADXL345 sensor.
    """
    sensor_info = None
    for uuid, component_attr in zip(setup_dict.keys(), setup_dict.values()):
        if (
            component_attr["type"] == "sensor"
            and component_attr["name"] == "accelerometer"
        ):
            if sensor_info is None:
                sensor_info = {
                    "ID": uuid,
                    "name": component_attr["name"],
                    "range_min": component_attr["range"]["min"],
                    "range_max": component_attr["range"]["max"],
                    "frequency": component_attr["frequency"]["value"],
                }
            else:
                print(
                    "Make sure that only one accelerometer is specified in the setup JSON"
                )
    if sensor_info is None:
        raise RuntimeError(
            "The Accelerometer is not included in the setup JSON, or unknown 'type' and 'name' are used."
        )
    return sensor_info


def get_general_info(setup_dict: Dict) -> Dict:
    """Get general metadata from JSON setup file.

    Args:
        setup_dict (Dict): Contains information from JSON setup file.

    Raises:
        RuntimeError: Occurs if no suitable information was found in the JSON setup file.

    Returns:
        Dict: Contains the name of the researcher conducting the experiment and the name of the experiment.
    """
    researcher = None
    for component_attr in setup_dict.values():
        if component_attr["type"] == "general":
            if researcher is None:
                with open(component_attr["path"], "r") as general_json_file:
                    json_content = json.load(general_json_file)
                researcher = {
                    "researcher": json_content["general"]["researcher"],
                    "experiment": json_content["general"]["experiment"],
                }
            else:
                print(
                    "Make sure that only one general-JSON file is specified in the setup JSON"
                )
    if researcher is None:
        raise RuntimeError(
            "The 'general' JSON file is not included in the setup JSON, or the 'general' JSON file uses the wrong keys."
        )
    return researcher


def get_probe_info(setup_dict: Dict) -> str:
    """Get information about probe from JSON setup file.

    Args:
        setup_dict (Dict): Contains information from JSON setup file.

    Raises:
        RuntimeError: Occurs if no suitable information was found in the JSON setup file.

    Returns:
        str: Name of the analysed probe, as specified in the JSON setup file.
    """
    probe_name = None
    for component_attr in setup_dict.values():
        if component_attr["type"] == "probe":
            if probe_name is None:
                probe_name = component_attr["name"]
            else:
                print(
                    "Make sure that only one general-JSON file is specified in the setup JSON"
                )
    if probe_name is None:
        raise RuntimeError(
            "The probe is not included in the setup JSON, or unknown 'type' is used."
        )
    return probe_name


def generate_safe_name(researcher: str, probe: str) -> Dict:
    """Generates a name for directory and h5-file containing the measurment data.

    Args:
        researcher (str): Name of the researcher conducting the experiment
        probe (str): Name of the analysed probe, as specified in the JSON setup file.

    Returns:
        Dict: Containing filename (key 'filename') and path to file (key 'foldername')
    """

    safetime = time.localtime(time.time())
    iso = time.strftime("%Y%m%d_%H%M%S", safetime)

    foldername = "data_" + iso + "_" + researcher + "_" + probe
    filename = foldername + ".h5"
    foldername = os.path.join("measurement_data", foldername)
    os.makedirs(foldername)
    return {"filename": filename, "foldername": foldername}


def createH5File(filename: str, groupInfos: Dict, folder: str) -> Tuple[str, str]:
    """Creates h5-file and adds some attributes.

    Args:
        filename (str): Name of the h5-file to be created.
        groupInfos (Dict): Dictionary with the general information, created with get_general_info().
        folder (str): Path to the directory where the h5-file will be created.

    Returns:
        path_to_file (str): The file path to the HDF5 file where data should be logged.
        folder (str): Path to the folder where data should be logged.
    """
    path_to_file = os.path.join(folder, filename)

    with h5py.File(path_to_file, "w") as h5_file:
        h5_file.attrs["created"] = time.asctime(time.localtime())
        h5_file.attrs["researcher"] = groupInfos["researcher"]
        h5_file.attrs["experiment"] = groupInfos["experiment"]

    return path_to_file, folder


def log_JSON(setup_dict: Dict, path_setup_JSON: str, target_folder: str):
    """Copies the relevant JSON metadata files to the directory where the measurement data is stored.

    Args:
        setup_dict (Dict): Contains all metadata from the setup-JSON file.
        path_setup_JSON (str): Path to the JSON-Setup file.
        target_folder (str): Path to the directory where the measurement data is stored.
    """
    if not os.path.exists(target_folder):
        os.makedirs(target_folder)

    for component_uuid, component_info in zip(setup_dict.keys(), setup_dict.values()):
        component_json_path = component_info["path"]
        new_file_path = os.path.join(target_folder, "{}.json".format(component_uuid))
        shutil.copyfile(component_json_path, new_file_path)

    with open(path_setup_JSON, "r") as jsonfile:
        jsonContent = json.load(jsonfile)
    new_file_path = os.path.join(
        target_folder, "{}.json".format(jsonContent["JSON"]["ID"])
    )
    shutil.copyfile(path_setup_JSON, new_file_path)


def set_sensor_setting(
    accelerometer: object, frequency: float, range_min: int, range_max: int
) -> object:
    """Changes data rate and data range of the ADCL345 sensor.

    Args:
        accelerometer (object): adafruit_adxl34x.ADXL345() object
        frequency (float): Frequency of the sensor
        range_min (int): Minimum range of the sensor
        range_max (int): Maximum range of the sensor

    Returns:
        object: adafruit_adxl34x.ADXL345() object with changed settings
    """
    import adafruit_adxl34x

    # in Hz to int
    sensor_frequency_dict = {
        3200: adafruit_adxl34x.DataRate.RATE_3200_HZ,
        1600: adafruit_adxl34x.DataRate.RATE_1600_HZ,
        800: adafruit_adxl34x.DataRate.RATE_800_HZ,
        400: adafruit_adxl34x.DataRate.RATE_400_HZ,
        200: adafruit_adxl34x.DataRate.RATE_200_HZ,
        100: adafruit_adxl34x.DataRate.RATE_100_HZ,
        50: adafruit_adxl34x.DataRate.RATE_50_HZ,
        25: adafruit_adxl34x.DataRate.RATE_25_HZ,
        12.5: adafruit_adxl34x.DataRate.RATE_12_5_HZ,
        6.25: adafruit_adxl34x.DataRate.RATE_6_25HZ,
        3.13: adafruit_adxl34x.DataRate.RATE_3_13_HZ,
        1.56: adafruit_adxl34x.DataRate.RATE_1_56_HZ,
        0.78: adafruit_adxl34x.DataRate.RATE_0_78_HZ,
        0.39: adafruit_adxl34x.DataRate.RATE_0_39_HZ,
        0.20: adafruit_adxl34x.DataRate.RATE_0_20_HZ,
        0.10: adafruit_adxl34x.DataRate.RATE_0_10_HZ,
    }
    # +/-g to int
    sensor_range_dict = {
        16: adafruit_adxl34x.Range.RANGE_16_G,
        8: adafruit_adxl34x.Range.RANGE_8_G,
        4: adafruit_adxl34x.Range.RANGE_4_G,
        2: adafruit_adxl34x.Range.RANGE_2_G,
    }
    # set range
    if -1 * range_min != range_max:
        print("Invalid sensor range. Default range is used.")
    elif range_max in sensor_range_dict.keys():
        accelerometer.range = sensor_range_dict[range_max]
    else:
        print("Invalid sensor range. Default range is used.")

    # set frequency
    if frequency in sensor_frequency_dict.keys():
        accelerometer.data_rate = sensor_frequency_dict[frequency]
    else:
        print("Invalid frequency range. Default frequency is used.")

    return accelerometer
