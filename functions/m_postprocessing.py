"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft, fftfreq
from typing import Tuple
import math
from scipy.interpolate import interp1d


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    x_acceleration = np.abs(x)
    y_acceleration = np.abs(y)
    z_acceleration = np.abs(z)
    
    i=0
    j=0
    
    zwischenergebnis=[]
    while(i<len(x_acceleration)):
        zwischenergebnis.append(math.sqrt(x_acceleration[i]*x_acceleration[i])+(y_acceleration[i]*y_acceleration[i]))
        i=i+1
        
    ergebnis=[]
    while(j<len(x_acceleration)):
        ergebnis.append(math.sqrt((zwischenergebnis[j]*zwischenergebnis[j])+(z_acceleration[j]*z_acceleration[j])))
        j=j+1
       
    return ergebnis


def interpolation(time: np.ndarray, val: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
        
    new_time_stamps = [0]
    time_dist = 10/len(val)
    i = 1

        
    while i<len(val):
        new_time_stamps.append(new_time_stamps[i-1]+time_dist)
        i=i+1
            
    # Erstelle eine lineare Interpolationsfunktion
    interpolate_func = interp1d(time, val, kind='linear', fill_value='extrapolate')
    
    # Berechne die interpolierten Beschleunigungswerte für neue Zeitstempel
    interpolated_acceleration = interpolate_func(new_time_stamps)
    
    return interpolated_acceleration


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    fft_result = fft(x)
    # Bestimmen der Frequenzen entsprechend der Samplingrate und der Länge des Signals
    frequencies = fftfreq(len(x), d=1/time)
    
    return frequencies,  fft_result
    